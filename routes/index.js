var express = require('express');
var router = express.Router();

var getCategories = require('../controllers/category/getCategories');
var getSubcategories = require('../controllers/subcategory/getSubcategories');

router.get('/categories', getCategories);
router.get('/subcategories', getSubcategories);

module.exports = router;
