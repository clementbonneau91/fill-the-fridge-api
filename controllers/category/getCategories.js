var db = require('../../models/index');

const getCategories = async (req, res) => {
    try {
        const results = await db.Category.findAll({
            include: [
                {
                    model: db.Subcategory,
                    as: 'subcategory',
                    attributes: ['id', 'title'],
                },
            ],
        });

        return res.status(200).json({ results });
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
};

module.exports = getCategories;
