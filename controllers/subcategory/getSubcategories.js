var db = require('../../models/index');

const getSubcategories = async (req, res) => {
    try {
        const results = await db.Subcategory.findAll({
            include: [
                {
                    model: db.Category,
                    as: 'category',
                    attributes: ['id', 'title', 'image'],
                },
                {
                    model: db.Product,
                    as: 'products',
                    attributes: ['id', 'title'],
                },
            ],
        });

        return res.status(200).json({ results });
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
};

module.exports = getSubcategories;
