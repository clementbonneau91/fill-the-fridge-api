module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define('Product', {
        title: DataTypes.STRING,
    }, {});

    Product.associate = function (models) {
        Product.belongsTo(models.Subcategory, {
            foreignKey: 'subcategory_id',
            as: 'subcategory',
        });
    };

    return Product;
};
