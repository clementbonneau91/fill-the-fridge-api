module.exports = (sequelize, DataTypes) => {
    const Subcategory = sequelize.define('Subcategory', {
        title: DataTypes.STRING,
        image: DataTypes.STRING,
    }, {});

    Subcategory.associate = function (models) {
        Subcategory.belongsTo(models.Category, {
            foreignKey: 'category_id',
            as: 'category',
        });
        Subcategory.hasMany(models.Product, {
            foreignKey: 'subcategory_id',
            as: 'products',
        });
    };

    return Subcategory;
};
