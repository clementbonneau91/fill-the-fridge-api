module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define('Category', {
        title: DataTypes.STRING,
        image: DataTypes.STRING,
    }, {});

    Category.associate = function (models) {
        Category.hasMany(models.Subcategory, {
            foreignKey: 'category_id',
            as: 'subcategory',
        });
    };

    return Category;
};
